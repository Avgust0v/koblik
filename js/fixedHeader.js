const header = document.querySelector('.header');
let prevScroll = window.scrollY;
let h = document.querySelector('.intro').offsetHeight;

window.addEventListener('scroll', () => {
    scroll = window.scrollY
    if (scroll < prevScroll && scroll > h) {
        header.classList.add('fixed')
    } else {
        header.classList.remove('fixed')
    }
    prevScroll = scroll;
});


// window.addEventListener('scroll', () => {
//     scroll = window.scrollY
//     if (scroll > 840) {
//         header.classList.add('header--background')
//     } else {
//         header.classList.remove('header--background')
//     }
//     prevScroll = scroll;
// });

