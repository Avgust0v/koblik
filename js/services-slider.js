$(document).ready(function () {
	$('.services-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		infinite: false,
		appendDots: $('.services-slider__dots'),
		prevArrow:
			"<button type='button' class='services-slider__slick-prev'></button>",
		nextArrow:
			"<button type='button' class='services-slider__slick-next'></button>",
	});

});
